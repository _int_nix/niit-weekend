#ifndef _CIRCLE_H_
#define _CIRCLE_H_


class Circle
{
private:
    double Radius;
    double Ference;
    double Area;
    double Pi = 3.1415926535;

public:
    Circle(): Radius(0), Ference(0), Area(0) {}
    double getShape() const;
    double getArea() const;
    double getRadius() const;
    double getFerence() const;
    double getPool(double radius, double cost_stone, double cost_fence);
    double getEarthAndRope(double lenght);
    void setRadius(double);
    void setFerence(double);
    void setArea(double);
    void reset();
};

#endif // CIRCLE_H
