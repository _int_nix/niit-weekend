#include <math.h>
#include "circle.h"


void Circle::setRadius(double Radius)
{
    this->Radius = Radius;
    Area = Pi * pow(Radius, 2.0);
    Ference = 2 * Pi * Radius;
}

void Circle::setArea(double Area)
{
    this->Area = Area;
    Radius = Ference / (2 * Pi);
    Ference = 2 * Pi * Radius;
}

void Circle::setFerence(double Ference)
{
    this->Ference = Ference;
    Radius = this->Ference / (2 * Pi);
    Area = Pi * pow(Radius, 2);
}

double Circle::getRadius() const
{
    return this->Radius;
}

double Circle::getFerence() const
{
    return this->Ference;
}

double Circle::getArea() const
{
    return this->Area;
}

void Circle::reset()
{
    this->Radius = 0;
    this->Ference = 0;
    this->Area = 0;
}

double Circle::getPool(double radius, double cost_stone, double cost_fence)
{
    reset();
    setRadius(radius);
    double area = this->getArea();
    setRadius(radius + 1.0);
    double stoneRoad = cost_stone * (this->getArea() - area);
    double fence = cost_fence * getFerence();

    double result = stoneRoad + fence;

    return result;
}

double Circle::getEarthAndRope(double lenght)
{
    double answer = lenght /(2 * Pi);
    return answer;
}
