#include <string>
#include <ctime>
#include <iostream>
#include "DateTime.h"

std::string Months[12] = { "January", "February", "March", "April",
"May", "June", "July", "August", "September",
"October", "November", "December" };

std::string Days[7] = { "Sunday","Monday", "Thuesday", "Wednesday", "Thirsday",
"Friday", "Saturday" };

DateTime::DateTime()
{
	time(&rawtime);
	this->timeinfo = localtime(&rawtime);
	this->day = timeinfo->tm_mday;
	this->month = timeinfo->tm_mon;
	this->year = timeinfo->tm_year;
}

DateTime::DateTime(DateTime & dt)
{
	this->day = dt.day;
	this->month = dt.month;
	this->year = dt.year;
	time(&rawtime);
	this->timeinfo = localtime(&rawtime);
}

DateTime::DateTime(unsigned int day, unsigned int month, unsigned int year)
{
	time(&rawtime);
	this->timeinfo = localtime(&rawtime);
	this->timeinfo->tm_mday = day;
	this->timeinfo->tm_mon = month - 1;
	this->timeinfo->tm_year = year - 1900;
	this->rawtime = mktime(timeinfo);
	this->day = timeinfo->tm_mday;
	this->month = timeinfo->tm_mon;
	this->year = timeinfo->tm_year;

}

void DateTime::printTomorrow()
{
	time_t tomorrow = this->rawtime + (3600 * 24);
	this->timeinfo = localtime(&tomorrow);
	std::cout << "Tomorrow : ";
	printDate();
}

void DateTime::printYesterday()
{
	time_t yesterday = this->rawtime - (3600 * 24);
	this->timeinfo = localtime(&yesterday);
	std::cout << "Yesterday : ";
	printDate();
}

void DateTime::printToday() 
{	
	this->timeinfo = localtime(&rawtime);
	std::cout << "Today : ";
	printDate();
}

void DateTime::printFuture(unsigned int nday)
{
	this->rawtime = rawtime + (nday * (3600 * 24));
	this->timeinfo = localtime(&rawtime);
	std::cout << "Future Date : ";
	printDate();
}

void DateTime::printPast(unsigned int nday)
{
	this->rawtime = rawtime - (nday * (3600 * 24));
	this->timeinfo = localtime(&rawtime);
	std::cout << "Past Date : ";
	printDate();
}

void DateTime::printMonth()
{
	this->timeinfo = localtime(&rawtime);
	std::cout << "Month : " << Months[timeinfo->tm_mon] << std::endl;
}

void DateTime::printWeekDay()
{
	this->timeinfo = localtime(&rawtime);
	std::cout << "WeekDay : " << Days[timeinfo->tm_wday] << std::endl;
}

unsigned int DateTime::calcDifference(DateTime &dt1, DateTime &dt2)
{
	time_t dif = difftime(dt1.rawtime, dt2.rawtime);
	time_t result = abs(dif);
	return result / 3600 / 24;
}

void DateTime::printDate()
{
	std::cout << 
	timeinfo->tm_mday << "." << 
	Months[timeinfo->tm_mon] << "." <<
	timeinfo->tm_year + 1900 << " " <<
	Days[timeinfo->tm_wday] << std::endl;
	
	timeinfo->tm_mday = this->day;
	timeinfo->tm_mon = this->month;
	timeinfo->tm_year = this->year;
	this->rawtime = mktime(timeinfo);
}