#ifndef _DATETIME_H_
#define _DATETIME_H_
#pragma warning(disable : 4996)

class DateTime
{
private:
	int day;
	int month;
	int year;
	time_t rawtime;
	struct tm * timeinfo;

public:
	DateTime();
	DateTime(DateTime & dt);
	DateTime(unsigned int day, unsigned int month, unsigned int year);
	void printToday();
	void printYesterday();
	void printTomorrow();
	void printFuture(unsigned int nday);
	void printPast(unsigned int nday);
	void printMonth();
	void printWeekDay();
	void printDate();
	unsigned int calcDifference(DateTime &dt1, DateTime &dt2);
};

#endif
