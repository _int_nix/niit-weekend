#include <ctime>
#include <string>
#include "DateTime.h"
#include <iostream>

using namespace std;

int main()
{
	DateTime dt;	// Конструктор по умолчанию
	dt.printDate();

	DateTime dt2(dt); // Конструктор копирования
	dt2.printDate();
	
	DateTime dt3(8,8,2016);
	cout << "Difference : " << dt.calcDifference(dt2, dt3) << endl;
	dt3.printDate();
	dt3.printToday();

}
