#ifndef _AUTOMATA_H_
#define _AUTOMATA_H_
#include <string>

class Automata
{
private:
	unsigned int cash = 0;
	unsigned int product_cost = 0;
	int dif = 0;

	std::string menu[4] = {"Latte","Capuccino","Black","Mokachino"};
	int prices[4] = {20,40,15,35};
	enum States {OFF,WAIT,ACEPT,CHECK,COOK} state;
public:
	Automata();
	bool on();
	bool off();
	void coin(int num);
	bool check();
	void cancel();
	void cook();
	bool finish();
	void printMenu();
	void printState();
	void choice(unsigned int num);
};
#endif