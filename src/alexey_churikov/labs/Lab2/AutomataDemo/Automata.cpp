#include <iostream>
#include <string>
#include "Automata.h"


Automata::Automata()
{
	state = OFF;
}

bool Automata::on()
{
	state = WAIT;
	//printState();
	return true;
}

bool Automata::off()
{
	state = OFF;
	//printState();
	return true;
}

void Automata::coin(int num)
{
	if (state < WAIT)
		return;
	else if (num == 0)
		return;
	else if (num < 0)
		return;
	cash += num;
}


bool Automata::check()
{
	if (state < WAIT)
		return false;
	if (product_cost == 0)
		return false;
	dif = cash - product_cost;
	if (dif < 0)
		return false;

	state = CHECK;
	return true;

}

void Automata::cook()
{
	if (state < CHECK)
		return;
	state = COOK;
	std::cout << " You coffee is done. " ;
}

bool Automata::finish()
{
	if (state < COOK)

		return false;
	state = WAIT;
	if (dif == 0) 
		std::cout << "Have a nice day! " << std::endl;
	else if(dif > 0) 
		std::cout << "Take your change : " << abs(dif) << ".p" << std::endl;
	//printState();
	return true;
}

void Automata::cancel()
{
	if (state < ACEPT || cash == 0)
		return;
	state = WAIT;
	std::cout << "Not Enough Money. Need : " << dif << std::endl;
	//printState();
}

void Automata::printMenu()
{
	if (state < WAIT)
		return;
	int len = sizeof(menu)/ sizeof(menu[0]);
	std::cout << "Prices : " << std::endl;
	for (int i = 0; i < len; i++)
		std::cout << i+1 << ". " << menu[i] << " --- " << prices[i] << "p " << std::endl;
		
}

void Automata::choice(unsigned int num)
{
	if (state < WAIT)
		return;
	if (num == 0)
		return;
	int len = sizeof(menu) / sizeof(menu[0]);
	if (num > len)
	{
		std::cout << "Wrong number of selected product" << std::endl;
		state = WAIT;
		//printState();
		return;
	}
	std::cout << std::endl << "Your Choice : "
		<< menu[num - 1]  << std::endl 
		<< "Cost : " << prices[num - 1]
		<< "p " << std::endl;
	product_cost = prices[num-1];
	state = ACEPT;
	//printState();
}

void Automata::printState()
{
	switch (state)
	{
	case 0:
		std::cout << "STATE : OFF" << std::endl;
		break;
	case 1:
		std::cout << "STATE : WAIT" << std::endl;
		break;
	case 2:
		std::cout << "STATE : ACCEPT" << std::endl;
		break;
	case 3:
		std::cout << "STATE : CHECK" << std::endl;
		break;
	case 4:
		std::cout << "STATE : COOK" << std::endl;
		break;
	default:
		break;
	}
}