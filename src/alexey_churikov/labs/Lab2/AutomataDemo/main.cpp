#include "Automata.h"
#include <iostream>

using namespace std;

int main()
{
	Automata at;
	at.on();
	at.printMenu();
	at.choice(4);
	at.coin(36);
	if (at.check())
		at.cook();
	else at.cancel();
	at.finish();


	return 0;
}